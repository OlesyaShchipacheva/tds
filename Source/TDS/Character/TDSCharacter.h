// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../FuncLibrary/TDSTypes.h"
#include "TDSCharacter.generated.h"

//class UKismetSystemLibrary;

UCLASS(Blueprintable)
class ATDSCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ATDSCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;
	//��������� �������������� ������
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;


public:
	//Movement
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EMovementState MovementState;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementInfo;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool SprintRunEnabled;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool WalkEnabled;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool AimEnabled;
	UPROPERTY(EditAnywhere, Category = "Movement")
		float RotationSpeed;

	
		float LastRotatorYaw;
		float AxisX;
		float AxisY;

	UFUNCTION (BlueprintCallable)
		void RotateToCursor (float DeltaTime);
	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState();


	//Camera|Main

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Main")
		float HeightCameraMin;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Main")
		float HeightCameraMax;

	//Camera|Slide

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Slide")
		float HeightCameraChangeDistance;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Slide")
		float HeightCameraChangeSlideStepDistance;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Slide")
		float TimerStep;

	float CurrentSlideDistance;
	FTimerHandle SlideTimer;
	//Camera slide is finished
	bool IsSlideDone;
	//The camera zooms in on the character
	bool IsSlideUp;

	
	//Sprint 

	//FVector SprintDirection;

	//Stamina
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Stamina")
		bool StaminaRecoveryEnabled;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		int StaminaMax;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		int StaminaCurrentValue;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		int StaminaRecoveryValue;
	//the number by which stamina decreases when sprinting.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		int	StaminaConsumptionValue;

	FTimerHandle StaminaRecoveryTimer;

	
	UFUNCTION(BlueprintCallable)
		void StaminaSpend();
	UFUNCTION()
		bool CanSprint();
	UFUNCTION()
		void StartingStaminaRecovery();


	//Input Func
	UFUNCTION()
		void InputAxisX(float Value);
	UFUNCTION()
		void InputAxisY(float Value);
	UFUNCTION()
		void InputAxisCameraSlide(float Value);
	UFUNCTION()
		void OnRightMouseButtonPressed();
	UFUNCTION()
		void OnRightMouseButtonReleased();
	UFUNCTION()
		void OnMovementModeChangeWalkPressed();
	UFUNCTION()
		void OnMovementModeChangeWalkReleased();
	UFUNCTION()
		void OnMovementModeChangeSprintPressed();
	UFUNCTION()
		void OnMovementModeChangeSprintReleased();

	//Tick Func
	UFUNCTION()
		void MovementTick(float DeltaTime);
	UFUNCTION()
		void CameraSlideSmoothTick();

	UFUNCTION()
		void StaminaRecoveryTick();

};

