// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Engine/Engine.h"
#include "../Game/TDSPlayerController.h"


ATDSCharacter::ATDSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = false; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	AxisX = 0.0f;
	AxisY = 0.0f;
	RotationSpeed = 500.0f;
	MovementState = EMovementState::Run_State;
	SprintRunEnabled = false;
	WalkEnabled = false;
	AimEnabled = false;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprints/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
	

	//Configure camera zoom
	HeightCameraMin = 500.0f;
	HeightCameraMax = 1500.0f;
	HeightCameraChangeDistance = 300.0f;
	HeightCameraChangeSlideStepDistance = 1.0f;
	CurrentSlideDistance = 600.0f;
	TimerStep = 0.001f;
	IsSlideDone = true;
	IsSlideUp = false;

	//Configure stamina
	StaminaMax = 300;
	StaminaCurrentValue = StaminaMax;
	StaminaConsumptionValue = 1;
	StaminaRecoveryValue = 1;
	StaminaRecoveryEnabled = false;
}

void ATDSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);

			
			//�������� ���� �� ��������������� �������
			if (StaminaCurrentValue < StaminaMax && !StaminaRecoveryEnabled)
			{			
				StartingStaminaRecovery();
			}
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Stamina: %d / %d"), StaminaCurrentValue, StaminaMax));

			MovementTick(DeltaSeconds);
			//GEngine->AddOnScreenDebugMessage (-1, 5.f, FColor::Red, FString::Printf (TEXT ("Len: %f %f"), dir.X*180,dir.Y*180));
		}
	}
	
}

void ATDSCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &ATDSCharacter::InputAxisX);
	PlayerInputComponent->BindAxis("MoveRight", this, &ATDSCharacter::InputAxisY);
	PlayerInputComponent->BindAction("RightMouseButton", IE_Pressed, this, &ATDSCharacter::OnRightMouseButtonPressed);
	PlayerInputComponent->BindAction("RightMouseButton", IE_Released, this, &ATDSCharacter::OnRightMouseButtonReleased);
	PlayerInputComponent->BindAction("MovementModeChangeWalk", IE_Pressed, this, &ATDSCharacter::OnMovementModeChangeWalkPressed);
	PlayerInputComponent->BindAction("MovementModeChangeWalk", IE_Released, this, &ATDSCharacter::OnMovementModeChangeWalkReleased);
	PlayerInputComponent->BindAction("MovementModeChangeSprint", IE_Pressed, this, &ATDSCharacter::OnMovementModeChangeSprintPressed);
	PlayerInputComponent->BindAction("MovementModeChangeSprint", IE_Released, this, &ATDSCharacter::OnMovementModeChangeSprintReleased);

	PlayerInputComponent->BindAxis("MouseWheel", this, &ATDSCharacter::InputAxisCameraSlide);
}

void ATDSCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}
void ATDSCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATDSCharacter::InputAxisCameraSlide(float Value)
{
	if (IsSlideDone && CameraBoom != nullptr && Value!=0)
	{
		if (Value < 0)
		{
			// zoom out in the camera
			if ((CameraBoom->TargetArmLength + HeightCameraChangeDistance) <= HeightCameraMax)
			{
				IsSlideUp = true;
				IsSlideDone = false;
				SlideTimer = UKismetSystemLibrary::K2_SetTimer(this, FString("CameraSlideSmoothTick"), TimerStep, true);
			}				
		}
		else
		{
			// zoom out on the camera
			if ((CameraBoom->TargetArmLength - HeightCameraChangeDistance) >= HeightCameraMin)
			{
				IsSlideUp = false;
				IsSlideDone = false;
				SlideTimer = UKismetSystemLibrary::K2_SetTimer(this, FString("CameraSlideSmoothTick"), TimerStep, true);
			}
		}
	}
}

void ATDSCharacter::OnRightMouseButtonPressed()
{
	AimEnabled = true;
	ChangeMovementState();
}
void ATDSCharacter::OnRightMouseButtonReleased()
{
	AimEnabled = false;
	ChangeMovementState();
}

// the character is walking
//Press "Left Alt"
void ATDSCharacter::OnMovementModeChangeWalkPressed()
{
	WalkEnabled = true;
	ChangeMovementState();
}

//the character has stopped walking
void ATDSCharacter::OnMovementModeChangeWalkReleased()
{
	WalkEnabled = false;
	ChangeMovementState();
}

//Sprint on
//Press "Left Shift"
void ATDSCharacter::OnMovementModeChangeSprintPressed()
{
	if (CanSprint() && !SprintRunEnabled)
	{
		//SprintDirection = GetActorForwardVector();
		SprintRunEnabled = true;
	}
	ChangeMovementState();
}
//Sprint off
void ATDSCharacter::OnMovementModeChangeSprintReleased()
{
	SprintRunEnabled = false;
	ChangeMovementState();
}

void ATDSCharacter::CameraSlideSmoothTick()
{
	CurrentSlideDistance += HeightCameraChangeSlideStepDistance;
	if (IsSlideUp)
		CameraBoom->TargetArmLength += HeightCameraChangeSlideStepDistance;
	else
		CameraBoom->TargetArmLength -= HeightCameraChangeSlideStepDistance;

	if (CurrentSlideDistance >= HeightCameraChangeDistance) 
	{
		CurrentSlideDistance = 0.0f;
		IsSlideDone = true;
		UKismetSystemLibrary::K2_ClearAndInvalidateTimerHandle(this, SlideTimer);
	}

}

void ATDSCharacter::MovementTick(float DeltaTime)
{
	if(ATDSPlayerController* PC = Cast<ATDSPlayerController> (GetController ()))
	{
		if(MovementState == EMovementState::SprintRun_State)
		{
			if(CanSprint ())
			{
				StaminaSpend();
				AddMovementInput (GetActorForwardVector (), 1.0f);
				if(PC->GetMoveToMouseCursor())
					RotateToCursor (DeltaTime);
				return;
			}
			else {
				SprintRunEnabled = false;
				ChangeMovementState ();
			}
		}
		//���� � ������ ������ ���������� �������� �� ��������
		if(!PC->GetMoveToMouseCursor ())
		{
			//Movement Forward and Back
			AddMovementInput (FVector (1.0f, 0.0f, 0.0f), AxisX);
			//Movement Right and Left
			AddMovementInput (FVector (0.0f, 1.0f, 0.0f), AxisY);
		}
		RotateToCursor (DeltaTime);
	}
}

void ATDSCharacter::ChangeMovementState()
{

	if (!WalkEnabled && !SprintRunEnabled && !AimEnabled/* && !StaminaRecoveryEnabled*/)
		MovementState = EMovementState::Run_State;
	else if (SprintRunEnabled)// && CanSprint())
	{
			WalkEnabled = false;
			AimEnabled = false;
			MovementState = EMovementState::SprintRun_State;
	}
	else if (WalkEnabled && AimEnabled)
		MovementState = EMovementState::AimWalk_State;
	else if (!WalkEnabled && AimEnabled)
		MovementState = EMovementState::Aim_State;
	else if ((WalkEnabled && !AimEnabled) /* || StaminaRecoveryEnabled*/)
		MovementState = EMovementState::Walk_State;
	else if(StaminaRecoveryEnabled)
	{
		SprintRunEnabled = false;
		MovementState = EMovementState::Walk_State;
	}
	else
		MovementState = EMovementState::Run_State;
	
	CharacterUpdate();

	
}

void ATDSCharacter::RotateToCursor (float DeltaTime)
{
	//������������� ���������
	FVector CursorLocation = GetCursorToWorld()->GetComponentLocation();		
	if (CursorLocation.X != 0.0f && CursorLocation.Y != 0.0f)
	{
		LastRotatorYaw = UKismetMathLibrary::RInterpTo_Constant(GetActorRotation(),
																UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), CursorLocation),
																DeltaTime,
																RotationSpeed).Yaw;
	}

	SetActorRotation(FRotator(0.0f, LastRotatorYaw, 0.0f));
}

void ATDSCharacter::CharacterUpdate()
{
	float ResultSpeed = 600.0f;
	
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResultSpeed = MovementInfo.AimSpeedNormal;
		break;
	case EMovementState::AimWalk_State:
		ResultSpeed = MovementInfo.AimSpeedWalk;
		break;
	case EMovementState::Walk_State:
		ResultSpeed = MovementInfo.WalkSpeedNormal;
		break;
	case EMovementState::Run_State:
		ResultSpeed = MovementInfo.RunSpeedNormal;
		break;
	case EMovementState::SprintRun_State:
		ResultSpeed = MovementInfo.SprintRunSpeedRun;
		break;
	default:
		ResultSpeed = MovementInfo.RunSpeedNormal;
		break;
	}
	GetCharacterMovement()->MaxWalkSpeed = ResultSpeed;
}

void ATDSCharacter::StaminaSpend()
{
	if (StaminaCurrentValue > 0)
		StaminaCurrentValue -= StaminaConsumptionValue;

}

void ATDSCharacter::StaminaRecoveryTick()
{
	if(StaminaCurrentValue < StaminaMax) {
		StaminaCurrentValue += StaminaRecoveryValue;
		
	}
	else if(StaminaCurrentValue > StaminaMax)
		StaminaCurrentValue = StaminaMax;

	

	if (StaminaCurrentValue == StaminaMax)
	{
		UKismetSystemLibrary::K2_ClearAndInvalidateTimerHandle(this, StaminaRecoveryTimer);
		StaminaRecoveryEnabled = false;
		ChangeMovementState();
	}
}

bool ATDSCharacter::CanSprint()
{
	if(StaminaCurrentValue > 0 && !StaminaRecoveryEnabled)
	{
		float dotProduct = FVector::DotProduct (GetActorForwardVector (), GetVelocity().GetSafeNormal());
		if(dotProduct >= 0.85)
			return true;
	}
	return false;
}

void ATDSCharacter::StartingStaminaRecovery()
{
	//��������� ����������� �� ��������
	if (!SprintRunEnabled || StaminaCurrentValue == 0)
	{
		StaminaRecoveryEnabled = true;
		//SprintRunEnabled = false;
		ChangeMovementState();
		StaminaRecoveryTimer = UKismetSystemLibrary::K2_SetTimer(this, FString("StaminaRecoveryTick"), 0.01f, true);
	}
}




